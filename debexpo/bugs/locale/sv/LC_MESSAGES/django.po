# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-11-22 21:27+0000\n"
"PO-Revision-Date: 2023-09-06 11:55+0000\n"
"Last-Translator: bittin1ddc447d824349b2 <bittin@reimu.nl>\n"
"Language-Team: Swedish <https://hosted.weblate.org/projects/debexpo/bugs/sv/>"
"\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.0.1-dev\n"

#: bugs/models.py
msgid "Package name"
msgstr "Paketnamn"

#: bugs/models.py
msgid "Bug number"
msgstr "Buggnummer"

#: bugs/models.py
msgid "Type"
msgstr "Typ"

#: bugs/models.py
msgid "Status"
msgstr "Status"

#: bugs/models.py
msgid "Severity"
msgstr "Allvarlighetsgrad"

#: bugs/models.py
msgid "Creation date"
msgstr "Skapande datum"

#: bugs/models.py
msgid "Last update date"
msgstr "Senaste uppdateringsdatum"

#: bugs/models.py
msgid "Subject"
msgstr "Ämne"

#: bugs/models.py
msgid "Submitter email"
msgstr "Insändarens e-post"

#: bugs/models.py
msgid "Owner email"
msgstr "Ägarens e-post"
