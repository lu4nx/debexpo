# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-11-22 21:27+0000\n"
"PO-Revision-Date: 2022-12-23 14:56+0000\n"
"Last-Translator: Luna Jernberg <droidbittin@gmail.com>\n"
"Language-Team: Swedish <https://hosted.weblate.org/projects/debexpo/"
"repository/sv/>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.15.1-dev\n"

#: repository/models.py
msgid "Name"
msgstr "Namn"

#: repository/models.py
msgid "Version"
msgstr "Version"

#: repository/models.py
msgid "Component"
msgstr "Komponent"

#: repository/models.py
msgid "Distribution"
msgstr "Distribution"

#: repository/models.py
msgid "Path"
msgstr "Sökväg"

#: repository/models.py
msgid "Size"
msgstr "Storlek"

#: repository/models.py
msgid "SHA256"
msgstr "SHA256"
