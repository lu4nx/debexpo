# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-11-22 21:27+0000\n"
"PO-Revision-Date: 2023-02-25 19:37+0000\n"
"Last-Translator: Andreas Hunkeler <ah+a77dj3@einbit.ch>\n"
"Language-Team: German <https://hosted.weblate.org/projects/debexpo/comments/"
"de/>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.16-dev\n"

#: comments/models.py
msgid "Not reviewed"
msgstr "Nicht geprüft"

#: comments/models.py
msgid "Needs work"
msgstr "Muss bearbeitet werden"

#: comments/models.py
msgid "Ready"
msgstr "Bereit"

#: comments/models.py
msgid "Name"
msgstr "Name"

#: comments/models.py
msgid "Subscribe to uploads"
msgstr "Abonniere Uploads"

#: comments/models.py
msgid "Subscribe to comments"
msgstr "Abonniere Kommentare"

#: comments/models.py
msgid "Comment"
msgstr "Kommentar"

#: comments/models.py
msgid "Comment date"
msgstr "Kommentar-Datum"

#: comments/models.py
msgid "Upload outcome"
msgstr "Upload Resultat"

#: comments/models.py
msgid "Uploaded to debian"
msgstr "Zu Debian hochgeladen"

#: comments/models.py
msgid "New comment on package {}"
msgstr "Neuer Kommentar im Paket {}"

#: comments/templates/subscribe.html
msgid "Subscribe to package "
msgstr "Abonniere Paket "

#: comments/templates/subscribe.html
msgid "Submit"
msgstr "Absenden"

#: comments/templates/subscribe.html
msgid "Back to package details"
msgstr "Zurück zu den Paket Details"

#: comments/templates/subscribe.html
msgid "Back to subscription list"
msgstr "Zurück zu Abonnementliste"

#: comments/templates/subscriptions.html
msgid "My subscription list"
msgstr "Meine Abonnementliste"

#: comments/templates/subscriptions.html
msgid "Package"
msgstr "Paket"

#: comments/templates/subscriptions.html
msgid "Notification on"
msgstr "Benachrichtigung ein"

#: comments/templates/subscriptions.html
msgid "Edit"
msgstr "Ändern"

#: comments/templates/subscriptions.html
msgid "Update"
msgstr "Aktualisierung"

#: comments/templates/subscriptions.html
msgid "Unsubscribe"
msgstr "Abmelden"

#: comments/templates/subscriptions.html
msgid "No subscriptions"
msgstr "Keine Abonnements"

#: comments/templates/subscriptions.html
msgid "Add new subscription"
msgstr "Neues Abonnement hinzufügen"

#: comments/templates/subscriptions.html
msgid "Package Name:"
msgstr "Paketname:"

#: comments/templates/subscriptions.html
msgid "Create subscription"
msgstr "Abonnement erstellen"
